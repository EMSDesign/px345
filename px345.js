// VARIABLES
const MODBUS_IP = '192.168.1.56';
const MODBUS_PORT = 502;
const SEND_MODBUS_INTERVAL = 30; //ms
// END OF VARIABLES

// Create a TCP modbus client
const Modbus = require('jsmodbus');
const net = require('net');
const socket = new net.Socket();
const client = new Modbus.client.TCP(socket, 0);

// On connect listener
socket.on('connect', function () {

    // Start transmit data from 0
    let VALUE = 0;

    // Increment speed
    let INCREMENT = 1;

    // Minimum value
    const MIN_VALUE = 0;

    // Maximum value
    const MAX_VALUE = 255;

    // Create loop
    setInterval(function() {
        VALUE = VALUE + INCREMENT;

        if(VALUE > MAX_VALUE) {
            VALUE = MAX_VALUE;
            INCREMENT = -INCREMENT;
        }

        if(VALUE < MIN_VALUE) {
            VALUE = MIN_VALUE;
            INCREMENT = -INCREMENT;
        }

        // Show debug logs
        // VALUE / DATE_TIME
        console.log('')
        console.log(new Date())
        console.log('Value ' + VALUE + ' prepared')

        // Used multipleRegisters to show where the problem is
        // For that example sending only single register but with multipleRegisters command
        client.writeMultipleRegisters(1, [VALUE]).then(function (resp) {
            // Show debug logs
            // VALUE / DATE_TIME
            console.log('')
            console.log(new Date())
            console.log('Value ' + VALUE + ' correctly sent')
        }, console.error);
    }, SEND_MODBUS_INTERVAL)
})

socket.connect({
    'host' : MODBUS_IP,
    'port' : MODBUS_PORT
})
